import {configureStore} from "@reduxjs/toolkit";
import getReducer from "./AllReducers";

export const store = configureStore({
    reducer: {
        GetAllData: getReducer
    },
    middleware: getDefaultMiddleware => getDefaultMiddleware({serializableCheck:false})
})

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;