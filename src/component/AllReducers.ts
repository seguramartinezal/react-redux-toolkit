import {createSlice, createAsyncThunk} from "@reduxjs/toolkit";
import axios from "axios";

export const GetAll = createAsyncThunk("GetAll", async () => {

    try {
        const response = await axios.get("http://localhost:5000/");
        return response.data;

    }catch (error:any){
        console.log(error);
    }
})

export const PostItem = createAsyncThunk("PostItem", async (data:any) => {

    try {
        const response = await axios.post("http://localhost:5000/", data);
        return response.data;

    }catch (error:any){
        console.log(error);
    }
})

export const DeleteItem = createAsyncThunk("DeleteItem", async (data:any) => {

    try {
        const response = await axios.delete("http://localhost:5000/", {data:{data}});
        return response.data;

    }catch (error:any){
        console.log(error);
    }
})

export const UpdateItem = createAsyncThunk("UpdateItem", async (data:any) => {
    try {
        const response = await axios.put("http://localhost:5000/", data);
        return response.data;

    }catch (error:any){
        console.log(error);
    }
})

const initialState = {
    status: "",
    details: [],
    name: "",
    lastName: ""
}

export const GetAllSlice = createSlice({
    name: "AllItems",
    initialState,
    reducers: {

    },
    extraReducers: builder => {
        //GET
        builder.addCase(GetAll.pending, (state, action) => {
            state.status = "Pending";
        })
        builder.addCase(GetAll.fulfilled, (state, action) => {
            state.status = "Success";
            state.details = action.payload;
        })
        builder.addCase(GetAll.rejected, (state, action) => {
            state.status = "Rejected";
        })

        //POST
        builder.addCase(PostItem.pending, (state, action) => {
            state.status = "Pending";
        })
        builder.addCase(PostItem.fulfilled, (state, action) => {
            state.status = "Success";
        })
        builder.addCase(PostItem.rejected, (state, action) => {
            state.status = "Rejected";
        })

        //DELETE
        builder.addCase(DeleteItem.pending, (state, action) => {
            state.status = "Pending";
        })
        builder.addCase(DeleteItem.fulfilled, (state, action) => {
            state.status = "Success";
        })
        builder.addCase(DeleteItem.rejected, (state, action) => {
            state.status = "Rejected";
        })

        //PUT
        builder.addCase(UpdateItem.pending, (state, action) => {
            state.status = "Pending";
        })
        builder.addCase(UpdateItem.fulfilled, (state, action) => {
            state.status = "Success";
        })
        builder.addCase(UpdateItem.rejected, (state, action) => {
            state.status = "Rejected";
        })

    }
})

export default GetAllSlice.reducer;