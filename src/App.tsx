import React, {useEffect, useState} from 'react';
import {useDispatch} from "react-redux";
import "bootstrap/dist/css/bootstrap.min.css";
import {DeleteItem, GetAll, PostItem, UpdateItem} from "./component/AllReducers";
import {AppDispatch} from "./component/store";
import Form from "./Form";
import RunArray from "./RunArray";
import Modall from "./Modall";

const Main = () => {

    const [id, setId] = useState<Number>();
    const [nombre, setNombre] = useState<string>();
    const [apellido, setApellido] = useState<string>();
    const [items, setItems] = useState<any[]>();
    const [isOpenModal, setIsOpenModal] = useState<boolean>(false);
    const dispatch = useDispatch<AppDispatch>();

    useEffect(() => {
        dispatch(GetAll()).then((obj:any) => {
            setItems(obj.payload);
        });
    }, [])

  const handleChange = ({target} : {target:any}) => {

        if(target.name === "nombre"){
            setNombre(target.value)
        }else if (target.name === "apellido"){
            setApellido(target.value);
        }
  }

  const handleSubmit = (e:any) => {
      e.preventDefault();
      e.target.reset();

      let obj = {
          id: 0,
          nombre: nombre,
          apellido: apellido
      }

      dispatch(PostItem(obj)).then((Allitems:any) => {
          setItems(Allitems.payload)
      });
  }

  const handleSubmitModal = (e:any) => {
      e.preventDefault();
  }

  const showModalandLoadData = (data:any) => {
      setIsOpenModal(!isOpenModal);
      setId(data.id);
      setNombre(data.nombre);
      setApellido(data.apellido);

  }

  const handleUpdateorDelete = ({target} : {target:any}) => {
        if(target.value === "delete"){
            Delete();
        }
        if(target.value === "update"){
            Update();
        }
    }

    const Delete = () => {

        dispatch(DeleteItem(id)).then((Allitems:any) => {
            setItems(Allitems.payload)
        });

        setIsOpenModal(!isOpenModal);
    }

    const Update = () => {

        const obj = {
            id: id,
            nombre: nombre,
            apellido: apellido
        }

        dispatch(UpdateItem(obj)).then((Allitems:any) => {
            setItems(Allitems.payload)
        });

       setIsOpenModal(!isOpenModal);
    }


    return (
      <>
          <nav className="navbar navbar-light bg-light">
              <div className="container-fluid">
                  <span className="navbar-brand mb-0 h1">CRUD con Redux-ToolKit en Runtime</span>
              </div>
          </nav>
      <Form handleChange={handleChange} handleSubmit={handleSubmit}/>
          
          <br/>
          <RunArray ArrayItems={items} showModal={showModalandLoadData}/>

          <Modall nombre={nombre} apellido={apellido} isOpenModal={isOpenModal} showModalandLoadData={showModalandLoadData}
                  handleUpdateorDelete={handleUpdateorDelete}
                  handleSubmitModal={handleSubmitModal} handleChange={handleChange}/>

      </>
  )
}


export default Main;