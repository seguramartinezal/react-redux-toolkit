import React from "react";

const RunArray = ({ArrayItems, showModal} : {ArrayItems: any, showModal:any}) => {
    return (
        <>
            <table className="table table-striped">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Apellido</th>
                </tr>
                </thead>
                <tbody>
                {ArrayItems?.map((x:any, index:number) => (

                    <tr key={index} onClick={() => showModal(x)}>
                        <th scope="row">{x.id}</th>
                        <td> {x.nombre}</td>
                        <td> {x.apellido}</td>

                    </tr>

                ))}
                </tbody>
            </table>
        </>
    )
}

export default RunArray;