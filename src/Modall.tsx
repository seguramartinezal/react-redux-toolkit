import React, {ChangeEventHandler, FormEventHandler} from "react";
import {Button, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";


const Modall = ({nombre, apellido, isOpenModal, showModalandLoadData, handleChange, handleSubmitModal, handleUpdateorDelete} :
                    {nombre:any, apellido:any, isOpenModal:any, showModalandLoadData:any, handleChange:ChangeEventHandler,
                        handleSubmitModal:FormEventHandler, handleUpdateorDelete:any}) => {
    return (
        <>
            <Modal isOpen={isOpenModal} toggle={showModalandLoadData}  backdrop={"static"}>
                <ModalHeader toggle={showModalandLoadData}>Actualizar/Borrar Datos</ModalHeader>
                <ModalBody>

                    <form onSubmit={handleSubmitModal} >
                        <div className={"form-floating"}>
                            <input placeholder={"Ingrese su nombre"} type={"text"} name={"nombre"}
                                   defaultValue={nombre} onChange={handleChange}  className={"form-control"} /><br/>
                            <label htmlFor={"Nombre"}>Nombre</label>
                        </div>

                        <div className={"form-floating"}>
                            <input placeholder={"Ingrese su nombre"} type={"text"} name={"apellido"}
                                   defaultValue={apellido} onChange={handleChange}  className={"form-control"} /><br/>
                            <label htmlFor={"Apellido"}>Apellido</label>
                        </div>

                        <br/>
                        <ModalFooter>

                            <button className={"btn btn-success"} onClick={handleUpdateorDelete} type={"submit"}
                                    value={"update"}>Actualizar</button>

                            <button className={"btn btn-danger"} onClick={handleUpdateorDelete}
                                    value={"delete"}>Borrar</button>
                            <Button color="secondary" onClick={showModalandLoadData}>Cancelar</Button>

                        </ModalFooter>

                    </form>
                </ModalBody>

            </Modal>


        </>
    )
}

export default Modall;