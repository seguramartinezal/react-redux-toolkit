import React, {ChangeEventHandler, FormEventHandler} from "react";

const Form = ({handleChange, handleSubmit} : {handleSubmit:FormEventHandler, handleChange: ChangeEventHandler} ) => {
    return (
        <>

            <h2 style={{position:"relative", left:"17em", width:"20px", top:"18px"}}>Formulario</h2>

            <div className={"border"} style={{width:"30em", position:"relative", left:"25em", top:"1em"}}>

                <form onSubmit={handleSubmit} className={"form"} style={{width: "20em", position: "relative", left:"5em", top:"1em"}}>

                    <div className={"form-floating"}>

                        <input name={"nombre"} onChange={handleChange} placeholder={"Introduce un nombre"}
                               className={"form-control"}/><br/>
                        <label htmlFor="floatingInput">Nombre</label>
                    </div>

                    <div className={"form-floating"}>
                        <input name={"apellido"} onChange={handleChange} placeholder={"Introduce un apellido"}
                               className={"form-control"}/><br/><br/>
                        <label htmlFor="floatingInput">Apellido</label>
                        <button type={"submit"} className={"btn btn-primary"}
                                style={{position:"relative", left:"7.6em", bottom:"1.9em"}}>Enviar</button>

                    </div>

                </form>

            </div>
        </>
    )
}

export default Form;